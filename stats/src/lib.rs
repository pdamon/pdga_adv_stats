use std::collections::BTreeMap;

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct TournamentData {
    pub metadata: TournamentMetaData,
    pub data: Vec<TournamentResult>,
}

impl TournamentData {
    pub fn new() -> TournamentData {
        TournamentData {
            metadata: TournamentMetaData {
                date_range: "".to_string(),
                location: "".to_string(),
                name: "".to_string(),
                divisions: vec![],
            },
            data: vec![],
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct TournamentMetaData {
    #[serde(rename = "DateRange")]
    pub date_range: String,
    #[serde(rename = "LocationShort")]
    pub location: String,
    #[serde(rename = "Name")]
    pub name: String,
    #[serde(rename = "Divisions")]
    pub divisions: Vec<Division>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Division {
    #[serde(rename = "Division")]
    pub name: String,
    #[serde(rename = "Players")]
    pub players: u32,
    #[serde(rename = "LayoutAssignments")]
    pub layout_assignments: BTreeMap<String, u32>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct TournamentResult {
    pub pdga_number: Option<u32>,
    pub player_name: String,
    pub division: String,
    pub location: Option<String>,
    pub rating: Option<u32>,

    pub score: u32,
    pub to_par: i32,
    pub round_rating: Option<u32>,
    pub place: u32,

    pub rounds_played: u32,

    pub hole_scores: Vec<HoleScore>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct HoleScore {
    // pub num: u32,
    pub par: u32,
    pub score: u32,
    pub distance: Option<u32>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct PlayerStatistics {
    pub name: String,
    pub location: String,
    pub birdies: RateStatistic,
    pub bogeys: RateStatistic,
    pub bob_plus: PercentileStatistic,
    pub bow_plus: PercentileStatistic,
    pub min: u32,
    pub low: u32,
    pub high: u32,
    pub max: u32,
    pub sg_short: PercentileStatistic,
    pub sg_mid: PercentileStatistic,
    pub sg_long: PercentileStatistic,
    pub sg_par_3s: PercentileStatistic,
    pub sg_par_4s: PercentileStatistic,
    pub sg_par_5s: PercentileStatistic,
}

impl PlayerStatistics {
    pub fn new() -> PlayerStatistics {
        PlayerStatistics {
            name: "".to_string(),
            location: "".to_string(),
            birdies: RateStatistic::new(),
            bogeys: RateStatistic::new(),
            bob_plus: PercentileStatistic::new(),
            bow_plus: PercentileStatistic::new(),
            min: 0,
            low: 5,
            high: 10,
            max: 15,
            sg_short: PercentileStatistic::new(),
            sg_mid: PercentileStatistic::new(),
            sg_long: PercentileStatistic::new(),
            sg_par_3s: PercentileStatistic::new(),
            sg_par_4s: PercentileStatistic::new(),
            sg_par_5s: PercentileStatistic::new(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct RateStatistic {
    pub successes: u32,
    pub attempts: u32,
    pub percentile_stat: PercentileStatistic,
}

impl RateStatistic {
    fn new() -> RateStatistic {
        RateStatistic {
            successes: 0,
            attempts: 0,
            percentile_stat: PercentileStatistic::new(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct PercentileStatistic {
    pub value: f32,
    pub percentile: f32,
}

impl PercentileStatistic {
    fn new() -> PercentileStatistic {
        PercentileStatistic {
            value: 0.0,
            percentile: 0.0,
        }
    }
}

/* ==================================================================
 * =                                                                =
 * =                 STATS PROCESSING FUNCTIONS                     =
 * =                                                                =
 * ================================================================== */

pub fn calc_player_statistics(results: Vec<TournamentResult>, name: String) -> PlayerStatistics {
    let player_result = results.iter().find(|x| x.player_name == name).unwrap();

    let (birdies, bob_plus) = calc_birdies(&results, player_result);
    let (bogeys, bow_plus) = calc_bogeys(&results, player_result);

    let (sg_par_3s, sg_par_4s, sg_par_5s) = calc_strokes_gained_by_par(&results, player_result);

    return match find_distance_cutoffs(&player_result.hole_scores) {
        Some((min, low, high, max)) => {
            let (sg_short, sg_mid, sg_long) =
                calc_strokes_gained_by_distance(&low, &high, &results, player_result);
            PlayerStatistics {
                name,
                location: player_result.location.clone().unwrap_or("".to_string()),
                birdies,
                bogeys,
                bob_plus,
                bow_plus,
                sg_short,
                sg_mid,
                sg_long,
                min,
                low,
                high,
                max,
                sg_par_3s,
                sg_par_4s,
                sg_par_5s,
            }
        }
        None => {
            let sg_short = PercentileStatistic {
                value: 0.0,
                percentile: 0.0,
            };
            let sg_mid = PercentileStatistic {
                value: 0.0,
                percentile: 0.0,
            };
            let sg_long = PercentileStatistic {
                value: 0.0,
                percentile: 0.0,
            };
            let min = 0;
            let low = 0;
            let high = 0;
            let max = 0;

            PlayerStatistics {
                name,
                location: player_result.location.clone().unwrap_or("".to_string()),
                birdies,
                bogeys,
                bob_plus,
                bow_plus,
                sg_short,
                sg_mid,
                sg_long,
                min,
                low,
                high,
                max,
                sg_par_3s,
                sg_par_4s,
                sg_par_5s,
            }
        }
    };
}

fn calc_birdies(
    results: &Vec<TournamentResult>,
    player_result: &TournamentResult,
) -> (RateStatistic, PercentileStatistic) {
    let player_birdies = player_result
        .hole_scores
        .iter()
        .filter(|x| x.score < x.par)
        .count();
    let player_birdie_rate =
        (player_birdies as f32 / player_result.hole_scores.len() as f32) * 100.0;

    let field_birdie_rates: Vec<f32> = results
        .iter()
        .map(|x| {
            (x.hole_scores.iter().filter(|y| y.score < y.par).count() as f32
                / x.hole_scores.len() as f32)
                * 100.0
        })
        .collect();
    let mean_birdie_rate = field_birdie_rates.iter().sum::<f32>() / field_birdie_rates.len() as f32;
    let birdies_percentile = calc_percentile(&field_birdie_rates, &player_birdie_rate);

    let bow_plus = calc_bob(&mean_birdie_rate, &player_birdie_rate);
    let field_bow_pluses = field_birdie_rates
        .iter()
        .map(|x| calc_bob(&mean_birdie_rate, x))
        .collect();
    let bow_plus_percentile = calc_percentile(&field_bow_pluses, &bow_plus);

    (
        RateStatistic {
            successes: player_birdies as u32,
            attempts: player_result.hole_scores.len() as u32,
            percentile_stat: PercentileStatistic {
                value: player_birdie_rate,
                percentile: birdies_percentile,
            },
        },
        PercentileStatistic {
            value: bow_plus,
            percentile: bow_plus_percentile,
        },
    )
}

fn calc_bogeys(
    results: &Vec<TournamentResult>,
    player_result: &TournamentResult,
) -> (RateStatistic, PercentileStatistic) {
    let player_bogeys = player_result
        .hole_scores
        .iter()
        .filter(|x| x.score > x.par)
        .count();

    let player_bogey_rate = (player_bogeys as f32 / player_result.hole_scores.len() as f32) * 100.0;

    let field_bogey_rates: Vec<f32> = results
        .iter()
        .map(|x| {
            (x.hole_scores.iter().filter(|y| y.score > y.par).count() as f32
                / x.hole_scores.len() as f32)
                * 100.0
        })
        .collect();

    let mean_bogey_rate = field_bogey_rates.iter().sum::<f32>() / field_bogey_rates.len() as f32;

    // negate the input values so less bogeys is a higher percentile value
    let bogeys_percentile = calc_percentile(
        &field_bogey_rates.iter().map(|x| -x).collect(),
        &&(-player_bogey_rate),
    );

    let bow_plus = calc_bow(&mean_bogey_rate, &player_bogey_rate);
    let field_bow_pluses = field_bogey_rates
        .iter()
        .map(|x| calc_bow(&mean_bogey_rate, x))
        .collect();
    let bow_plus_percentile = calc_percentile(&field_bow_pluses, &bow_plus);

    (
        RateStatistic {
            successes: player_bogeys as u32,
            attempts: player_result.hole_scores.len() as u32,
            percentile_stat: PercentileStatistic {
                value: player_bogey_rate,
                percentile: bogeys_percentile,
            },
        },
        PercentileStatistic {
            value: bow_plus,
            percentile: bow_plus_percentile,
        },
    )
}

fn calc_strokes_gained(results: &Vec<Vec<u32>>, player_result: &Vec<u32>) -> PercentileStatistic {
    let mean_strokes = results
        .iter()
        .map(|x| x.iter().sum())
        .collect::<Vec<u32>>()
        .iter()
        .sum::<u32>() as f32
        / results.len() as f32;

    let strokes_gained_by_player = results
        .iter()
        .map(|x| (mean_strokes - x.iter().sum::<u32>() as f32) / x.len() as f32)
        .collect();

    let strokes_gained_current_player =
        (mean_strokes - player_result.iter().sum::<u32>() as f32) / player_result.len() as f32;

    return PercentileStatistic {
        value: strokes_gained_current_player,
        percentile: calc_percentile(&strokes_gained_by_player, &strokes_gained_current_player),
    };
}

fn calc_percentile<T: PartialOrd + std::fmt::Debug>(values: &Vec<T>, x: &T) -> f32 {
    return (values.iter().filter(|i| x >= *i).count() as f32) / (values.len() as f32) * 100.0;
}

fn calc_bob(mean: &f32, value: &f32) -> f32 {
    value / mean * 100.0
}

fn calc_bow(mean: &f32, value: &f32) -> f32 {
    mean / value * 100.0
}

fn find_distance_cutoffs(holes: &Vec<HoleScore>) -> Option<(u32, u32, u32, u32)> {
    if holes[0].distance.is_none() {
        return None;
    }

    let mut mod_distances: Vec<u32> = holes
        .iter()
        .map(|x| (x.distance.unwrap() as f32 / x.par as f32).round() as u32)
        .collect();
    mod_distances.sort();

    let cutoff_low: usize = (0.33 * holes.len() as f32) as usize;
    let cutoff_high: usize = (0.66 * holes.len() as f32) as usize + 1;

    return Some((
        *mod_distances.first().unwrap(),
        mod_distances[cutoff_low],
        mod_distances[cutoff_high],
        *mod_distances.last().unwrap(),
    ));
}

fn scores_by_distances(
    low: &u32,
    high: &u32,
    holes: &Vec<HoleScore>,
) -> (Vec<u32>, Vec<u32>, Vec<u32>) {
    let short = holes
        .iter()
        .filter(|x| &((x.distance.unwrap() as f32 / x.par as f32).round() as u32) < low)
        .map(|x| x.score)
        .collect();
    let mid = holes
        .iter()
        .filter(|x| {
            (&((x.distance.unwrap() as f32 / x.par as f32).round() as u32) >= low)
                && (&((x.distance.unwrap() as f32 / x.par as f32).round() as u32) < high)
        })
        .map(|x| x.score)
        .collect();
    let long = holes
        .iter()
        .filter(|x| &((x.distance.unwrap() as f32 / x.par as f32).round() as u32) >= high)
        .map(|x| x.score)
        .collect();

    return (short, mid, long);
}

fn scores_by_par(holes: &Vec<HoleScore>) -> (Vec<u32>, Vec<u32>, Vec<u32>) {
    let threes = holes
        .iter()
        .filter(|x| x.par <= 3)
        .map(|x| x.score)
        .collect();
    let fours = holes
        .iter()
        .filter(|x| x.par == 4)
        .map(|x| x.score)
        .collect();
    let fives = holes
        .iter()
        .filter(|x| x.par >= 5)
        .map(|x| x.score)
        .collect();

    return (threes, fours, fives);
}

fn calc_strokes_gained_by_distance(
    low: &u32,
    high: &u32,
    results: &Vec<TournamentResult>,
    player_result: &TournamentResult,
) -> (
    PercentileStatistic,
    PercentileStatistic,
    PercentileStatistic,
) {
    let mut shorts = Vec::new();
    let mut mids = Vec::new();
    let mut longs = Vec::new();

    let (player_short, player_mid, player_long) =
        scores_by_distances(low, high, &player_result.hole_scores);

    for result in results.iter() {
        let (s, m, l) = scores_by_distances(low, high, &result.hole_scores);

        shorts.push(s);
        mids.push(m);
        longs.push(l);
    }

    let sg_short = calc_strokes_gained(&shorts, &player_short);
    let sg_mid = calc_strokes_gained(&mids, &player_mid);
    let sg_long = calc_strokes_gained(&longs, &player_long);

    return (sg_short, sg_mid, sg_long);
}

fn calc_strokes_gained_by_par(
    results: &Vec<TournamentResult>,
    player_result: &TournamentResult,
) -> (
    PercentileStatistic,
    PercentileStatistic,
    PercentileStatistic,
) {
    let mut threes = Vec::new();
    let mut fours = Vec::new();
    let mut fives = Vec::new();

    let (player_three, player_four, player_five) = scores_by_par(&player_result.hole_scores);

    for result in results.iter() {
        let (three, four, five) = scores_by_par(&result.hole_scores);

        threes.push(three);
        fours.push(four);
        fives.push(five);
    }

    let sg_three = calc_strokes_gained(&threes, &player_three);
    let sg_four = calc_strokes_gained(&fours, &player_four);
    let sg_five = calc_strokes_gained(&fives, &player_five);

    return (sg_three, sg_four, sg_five);
}

/* ==================================================================
 * =                                                                =
 * =                          UNIT TESTS                            =
 * =                                                                =
 * ================================================================== */

#[cfg(test)]
mod tests {
    use super::*;
    use assert_approx_eq::assert_approx_eq;
    use std::vec;

    fn make_test_result(scores: Vec<u32>) -> TournamentResult {
        assert_eq!(scores.len(), 7);

        return TournamentResult {
            pdga_number: Some(69420),
            player_name: "Fake Name".to_string(),
            division: "MPO".to_string(),
            location: Some("Planet Earth".to_string()),
            rating: None,
            score: scores.iter().sum(),
            to_par: 0,
            round_rating: None,
            place: 1,
            rounds_played: 2,
            hole_scores: vec![
                HoleScore {
                    par: 3,
                    score: scores[0],
                    distance: Some(10), // 3
                },
                HoleScore {
                    par: 5,
                    score: scores[1],
                    distance: Some(20), // 4
                },
                HoleScore {
                    par: 3,
                    score: scores[2],
                    distance: Some(16), // 5.33
                },
                HoleScore {
                    par: 4,
                    score: scores[3],
                    distance: Some(24), // 6
                },
                HoleScore {
                    par: 3,
                    score: scores[4],
                    distance: Some(14), // 4.66
                },
                HoleScore {
                    par: 3,
                    score: scores[5],
                    distance: Some(20), // 6.66
                },
                HoleScore {
                    par: 4,
                    score: scores[6],
                    distance: Some(20), // 5
                },
            ],
        };
    }

    fn make_field_results() -> Vec<TournamentResult> {
        vec![
            make_test_result(vec![3, 5, 3, 4, 3, 3, 4]),        // 0
            make_test_result(vec![2, 4, 3, 4, 1, 5, 4]),        // 3
            make_test_result(vec![4, 6, 3, 4, 3, 2, 4]),        // 1
            make_test_result(vec![10, 10, 10, 10, 10, 10, 10]), // 0
            make_test_result(vec![1, 1, 1, 1, 1, 1, 1]),        // 7
        ]
    }

    #[test]
    fn test_calc_birdies() {
        let player_result = make_test_result(vec![2, 4, 3, 4, 1, 3, 4]); // 3
        let field_results = make_field_results();

        let (birdies, bob_plus) = calc_birdies(&field_results, &player_result);

        assert_eq!(birdies.successes, 3);
        assert_eq!(birdies.attempts, 7);
        assert_approx_eq!(birdies.percentile_stat.value, 42.857, 0.001);
        assert_approx_eq!(birdies.percentile_stat.percentile, 80.0, 0.001);

        assert_approx_eq!(bob_plus.value, 136.364, 0.001);
        assert_approx_eq!(bob_plus.percentile, 80.0, 0.001);
    }

    #[test]
    fn test_calc_bogeys() {
        let player_result = make_test_result(vec![2, 4, 3, 4, 1, 4, 4]); // 1
        let field_results = make_field_results();

        let (bogeys, bob_plus) = calc_bogeys(&field_results, &player_result);

        assert_eq!(bogeys.successes, 1);
        assert_eq!(bogeys.attempts, 7);
        assert_approx_eq!(bogeys.percentile_stat.value, 14.286, 0.001);
        assert_approx_eq!(bogeys.percentile_stat.percentile, 60.0, 0.001);

        assert_approx_eq!(bob_plus.value, 200.0, 0.001);
        assert_approx_eq!(bob_plus.percentile, 60.0, 0.001);
    }

    #[test]
    fn test_calc_strokes_gained() {
        let strokes_taken = vec![vec![3, 3, 3], vec![2, 3, 4], vec![4, 4, 4]];
        let player_strokes_taken = vec![2, 3, 3];

        let result = calc_strokes_gained(&strokes_taken, &player_strokes_taken);

        assert_approx_eq!(result.value, 0.6666667);
        assert_approx_eq!(result.percentile, 100.0);
    }

    #[test]
    fn test_find_distance_cutoffs() {
        let input = make_test_result(vec![4, 4, 4, 4, 4, 4, 4]);
        assert_eq!(
            find_distance_cutoffs(&input.hole_scores),
            Some((3, 5, 6, 7))
        );
    }

    #[test]
    fn test_find_distance_cutoffs_null() {
        assert_eq!(
            find_distance_cutoffs(&vec![HoleScore {
                par: 3,
                score: 3,
                distance: None
            }]),
            None
        );
    }

    #[test]
    fn test_scores_by_distance() {
        let result = make_test_result(vec![1, 2, 3, 4, 5, 6, 7]);

        let (short, mid, long) = scores_by_distances(&5, &6, &result.hole_scores);

        assert_eq!(short, vec![1, 2]);
        assert_eq!(mid, vec![3, 5, 7]);
        assert_eq!(long, vec![4, 6])
    }

    #[test]
    fn test_scores_by_par() {
        let result = make_test_result(vec![1, 2, 3, 4, 5, 6, 7]);

        let (threes, fours, fives) = scores_by_par(&result.hole_scores);

        assert_eq!(threes, vec![1, 3, 5, 6]);
        assert_eq!(fours, vec![4, 7]);
        assert_eq!(fives, vec![2])
    }

    #[test]
    fn test_strokes_gained_by_distance() {
        let player_result = make_test_result(vec![2, 4, 3, 4, 1, 4, 4]);
        let field_results = make_field_results();

        // shorts are 1, 2, average is 4.6, player 3
        // mids are 3, 5, 7, average is 4.3333333, player 3.33333333
        // longs are 4, 6, average is 4.4, player is 4

        let (short, mid, long) =
            calc_strokes_gained_by_distance(&5, &6, &field_results, &player_result);

        assert_approx_eq!(short.value, 1.6);
        assert_approx_eq!(short.percentile, 80.0);

        assert_approx_eq!(mid.value, 1.4);
        assert_approx_eq!(mid.percentile, 80.0);

        assert_approx_eq!(long.value, 0.4);
        assert_approx_eq!(long.percentile, 40.0);
    }

    #[test]
    fn test_strokes_gained_by_par() {
        let player_result = make_test_result(vec![2, 4, 3, 4, 1, 4, 4]);
        let field_results = make_field_results();

        // threes are 1, 3, 5, 6 average is 3.95, player 2.5
        // fours are 4, 7 average is 4.6, player 4
        // fives are 2 average is 5.2, player is 4

        let (threes, fours, fives) = calc_strokes_gained_by_par(&field_results, &player_result);

        assert_approx_eq!(threes.value, 1.45);
        assert_approx_eq!(threes.percentile, 80.0);

        assert_approx_eq!(fours.value, 0.6);
        assert_approx_eq!(fours.percentile, 80.0);

        assert_approx_eq!(fives.value, 1.2);
        assert_approx_eq!(fives.percentile, 80.0);
    }

    #[test]
    fn test_calc_bob() {
        assert_approx_eq!(calc_bob(&4.3, &2.0), 46.51, 0.01);
        assert_approx_eq!(calc_bob(&2.0, &1.0), 50.0, 0.01);
        assert_approx_eq!(calc_bob(&2.7, &5.0), 185.19, 0.01);
    }

    #[test]
    fn test_calc_bow() {
        assert_approx_eq!(calc_bow(&4.3, &2.0), 215.0, 0.01);
        assert_approx_eq!(calc_bow(&2.0, &1.0), 200.0, 0.01);
        assert_approx_eq!(calc_bow(&2.7, &5.0), 54.0, 0.01);
    }
}
