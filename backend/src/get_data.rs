use stats::{HoleScore, TournamentMetaData, TournamentResult};

use std::{collections::BTreeMap, error::Error, fmt};

use cached::proc_macro::cached;
use futures::future::join_all;
use log::{error, info};
use reqwest;
use serde::{Deserialize, Serialize};

/* ======================================================================
 * =                                                                    =
 * =                         PDGA API RESPONSE STRUCTS                  =
 * =                                                                    =
 * ====================================================================== */

#[derive(Debug, Serialize, Deserialize)]
pub struct PDGAResponse {
    pub data: PDGAResponseType,
    pub hash: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum PDGAResponseType {
    Pool(PDGAPoolResults),
    Pools(Vec<PDGAPoolResults>),
    MetaData(TournamentMetaData),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AlwaysPool {
    pub data: PDGAPoolResults,
    pub hash: String,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct PDGAPoolResults {
    pub pool: String,
    pub division: String,
    pub layouts: Vec<PDGALayout>,
    pub scores: Vec<PDGATournamentResult>,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct PDGALayout {
    #[serde(rename = "Name")]
    name: String,
    #[serde(rename = "Holes")]
    num_holes: u32,
    #[serde(rename = "Units")]
    units: String,
    #[serde(rename = "Detail")]
    holes: Vec<PDGAHole>,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct PDGAHole {
    #[serde(rename = "Label")]
    hole: String,
    #[serde(rename = "Par")]
    par: u32,
    #[serde(rename = "Length")]
    length: Option<u32>,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
#[serde(untagged)]
pub enum StupidAssAPIWTFPDDGA {
    String(String),
    U32(u32),
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct PDGATournamentResult {
    #[serde(rename = "PDGANum")]
    pdga_number: Option<u32>,
    #[serde(rename = "Name")]
    player_name: String,
    #[serde(rename = "Division")]
    division: String,
    #[serde(rename = "FullLocation")]
    location: Option<String>,
    #[serde(rename = "Rating")]
    rating: Option<u32>,
    #[serde(rename = "RoundRating")]
    round_rating: Option<u32>,
    #[serde(rename = "RoundScore")]
    score: StupidAssAPIWTFPDDGA,
    #[serde(rename = "RoundtoPar")]
    to_par: i32,
    #[serde(rename = "Scores")]
    scores: String,
    #[serde(rename = "Pars")]
    pars: String,
    #[serde(rename = "Rounds")]
    rounds: String,
    #[serde(rename = "RunningPlace")]
    place: Option<u32>,
}

#[derive(Debug)]
pub enum PDGARespError {
    NotMetaData,
    FailedReadingRound,
}

impl Error for PDGARespError {}

impl fmt::Display for PDGARespError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            PDGARespError::NotMetaData => {
                write!(f, "Invalid Response. Expected PDGA Tournament Metadata.")
            }
            PDGARespError::FailedReadingRound => {
                write!(f, "Failed to read JSON of PDGA Round Response.")
            }
        }
    }
}

#[cached(result = true, sync_writes = true)]
pub async fn get_pdga_metadata(id: u32) -> Result<TournamentMetaData, Box<dyn Error>> {
    let uri = format!(
        "https://www.pdga.com/apps/tournament/live-api/live_results_fetch_event.php?TournID={}",
        id
    );

    info!("Getting data from url: {}", uri);
    let resp = reqwest::get(&uri).await?;
    info!("Got response: {:?}", resp);

    let text = resp.text().await?;
    let json_response: PDGAResponse = serde_json::from_str(&text)?;

    return match json_response.data {
        PDGAResponseType::MetaData(metadata) => Ok(metadata),
        _ => Err(Box::new(PDGARespError::NotMetaData)),
    };
}

pub fn make_divisions(metadata: TournamentMetaData) -> BTreeMap<String, Vec<u32>> {
    let mut return_value = BTreeMap::new();

    for division in metadata.divisions.into_iter() {
        let v = division
            .layout_assignments
            .keys()
            .map(|round| round.parse().unwrap())
            .collect();
        return_value.insert(division.name, v);
    }
    return return_value;
}

pub async fn get_round_results(
    id: u32,
    divisions: BTreeMap<String, Vec<u32>>,
) -> Result<BTreeMap<String, BTreeMap<u32, Vec<PDGAPoolResults>>>, Box<dyn Error>> {
    let mut round_getters = Vec::new();

    for (division, rounds) in divisions.into_iter() {
        for round in rounds.into_iter() {
            round_getters.push(get_round(id, division.clone(), round));
        }
    }

    let round_results = join_all(round_getters).await;

    let mut return_value: BTreeMap<String, BTreeMap<u32, Vec<PDGAPoolResults>>> = BTreeMap::new();

    for result in round_results.into_iter() {
        match result {
            Ok((round, division, pool_results)) => {
                insert_round_results(round, division, pool_results, &mut return_value);
            }
            Err(e) => return Err(e),
        }
    }

    return Ok(return_value);
}

#[cached(result = true, sync_writes = true)]
async fn get_round(
    id: u32,
    division: String,
    round: u32,
) -> Result<(u32, String, Vec<PDGAPoolResults>), Box<dyn Error>> {
    let uri = format!("https://www.pdga.com/apps/tournament/live-api/live_results_fetch_round.php?TournID={}&Division={}&Round={}", id, division, round);

    info!("Getting date from URI: {}", uri);
    let res = reqwest::get(&uri).await?.text().await?;
    info!("Got result {}\nfrom URI {}", res, uri);

    let results_raw: Result<PDGAResponse, serde_json::Error> = serde_json::from_str(&res);

    return match results_raw {
        Ok(v) => match v.data {
            PDGAResponseType::Pool(p) => Ok((round, division, vec![p])),
            PDGAResponseType::Pools(p) => Ok((round, division, p)),
            PDGAResponseType::MetaData(_) => Err(Box::new(PDGARespError::FailedReadingRound)),
        },
        Err(e) => {
            error!("{:?}", uri);
            error!("{:?}", res);
            error!("{:?}", e);

            Err(Box::new(e))
        }
    };
}

fn insert_round_results(
    round: u32,
    division: String,
    results: Vec<PDGAPoolResults>,
    map: &mut BTreeMap<String, BTreeMap<u32, Vec<PDGAPoolResults>>>,
) {
    match map.get_mut(&division) {
        Some(d) => {
            d.insert(round, results);
        }
        None => {
            let mut new_map = BTreeMap::new();

            new_map.insert(round, results);
            map.insert(division, new_map);
        }
    }
}

pub fn build_tournament_results(
    results: BTreeMap<String, BTreeMap<u32, Vec<PDGAPoolResults>>>,
) -> Vec<TournamentResult> {
    let mut return_value = Vec::new();

    for (_, rounds) in results.iter() {
        let mut rounds_to_results = BTreeMap::new();
        for (round, pool_results) in rounds {
            let combined_pools = build_tournament_round(pool_results.clone());
            rounds_to_results.insert(*round, combined_pools);
        }

        let division_results = combine_tournament_rounds(rounds_to_results);

        return_value.extend(division_results);
    }

    return return_value;
}

/*
    Combines multiple tournament rounds into one big single round for each player
*/
fn combine_tournament_rounds(
    results: BTreeMap<u32, Vec<TournamentResult>>,
) -> Vec<TournamentResult> {
    let mut group_by_player: BTreeMap<String, BTreeMap<u32, TournamentResult>> = BTreeMap::new();
    for (round, result) in results.into_iter() {
        for r in result.into_iter() {
            match group_by_player.get_mut(&r.player_name) {
                Some(v) => {
                    v.insert(round, r);
                }
                None => {
                    let mut new_map = BTreeMap::new();
                    new_map.insert(round, r.clone());
                    group_by_player.insert(r.player_name.clone(), new_map);
                }
            }
        }
    }

    let mut combined_rounds: Vec<TournamentResult> = Vec::new();

    for (_, result) in group_by_player.into_iter() {
        let (_, first_result) = result.iter().min_by(|(a, _), (b, _)| a.cmp(b)).unwrap();
        let (_, last_result) = result.iter().max_by(|(a, _), (b, _)| a.cmp(b)).unwrap();

        let rounds_played = result.iter().map(|(_, x)| x.rounds_played).max().unwrap();

        let tr = TournamentResult {
            pdga_number: first_result.pdga_number,
            player_name: first_result.player_name.clone(),
            division: first_result.division.clone(),
            location: first_result.location.clone(),
            rating: first_result.rating,
            score: result.iter().map(|(_, x)| x.score).sum(),
            to_par: result.iter().map(|(_, x)| x.to_par).sum(),
            rounds_played,
            round_rating: match (result
                .iter()
                .map(|(_, x)| x.round_rating.unwrap_or(0))
                .sum::<u32>() as f32
                / rounds_played as f32)
                .round() as u32
            {
                0 => None,
                v => Some(v),
            },
            place: last_result.place,
            hole_scores: result
                .iter()
                .map(|(_, x)| x.hole_scores.clone())
                .collect::<Vec<Vec<HoleScore>>>()
                .concat(),
        };

        combined_rounds.push(tr);
    }

    return combined_rounds;
}

/*
    Combines a vec of pool results into a vec of tournament results
*/
fn build_tournament_round(pools: Vec<PDGAPoolResults>) -> Vec<TournamentResult> {
    let mut results = Vec::new();
    for pool in pools.into_iter() {
        for result in pool.scores.into_iter() {
            results.push(convert_tournament_result(result, &pool.layouts[0]));
        }
    }

    return results;
}

/*
    Uses ad PDGATournamentResult and a PDGALayout to build a TournamentResult
*/
fn convert_tournament_result(
    result: PDGATournamentResult,
    layout: &PDGALayout,
) -> TournamentResult {
    let pars_raw: Vec<u32> = result
        .pars
        .split(",")
        .map(|x| x.parse().unwrap_or(0))
        .filter(|x| x > &0)
        .collect();

    let hole_scores: Vec<u32> = result
        .scores
        .split(",")
        .map(|x| x.parse().unwrap_or(0))
        .filter(|x| x > &0)
        .collect();

    let distances: Vec<Option<u32>> = layout.holes.iter().map(|x| x.length).collect();

    let hole_score_structs = match pars_raw.len() {
        0 => vec![],
        _ => {
            let pars = pars_raw[0..distances.len()].to_vec();

            assert_eq!(distances.len(), hole_scores.len());
            assert_eq!(distances.len(), pars.len());
            assert_eq!(distances.len(), layout.num_holes as usize);

            let mut hole_score_structs: Vec<HoleScore> = Vec::new();

            for (index, _) in pars.iter().enumerate() {
                hole_score_structs.push(HoleScore {
                    par: pars[index],
                    score: hole_scores[index],
                    distance: distances[index],
                });
            }

            hole_score_structs
        }
    };

    TournamentResult {
        pdga_number: result.pdga_number,
        player_name: result.player_name,
        division: result.division,
        location: result.location,
        rating: result.rating,
        score: match result.score {
            StupidAssAPIWTFPDDGA::String(s) => s.parse().unwrap(),
            StupidAssAPIWTFPDDGA::U32(u) => u,
        },
        to_par: result.to_par,
        place: result.place.unwrap_or(9999),
        round_rating: result.round_rating,
        rounds_played: result
            .rounds
            .split(",")
            .map(|x| x.parse().unwrap_or(0))
            .filter(|x| x > &0)
            .count() as u32,
        hole_scores: hole_score_structs,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn gen_layout() -> PDGALayout {
        PDGALayout {
            name: "Test Layout".to_string(),
            num_holes: 3,
            units: "Feet".to_string(),
            holes: vec![
                PDGAHole {
                    hole: 1.to_string(),
                    length: Some(12),
                    par: 223,
                },
                PDGAHole {
                    hole: 2.to_string(),
                    length: Some(20),
                    par: 175,
                },
                PDGAHole {
                    hole: 3.to_string(),
                    par: 75,
                    length: Some(33),
                },
            ],
        }
    }

    fn gen_tournament_result(
        name: &str,
        score1: u32,
        score2: u32,
        score3: u32,
        place: u32,
    ) -> TournamentResult {
        TournamentResult {
            pdga_number: Some(13216543),
            player_name: name.to_string(),
            division: "Flying".to_string(),
            location: None,
            rating: None,
            round_rating: None,
            score: 400,
            to_par: -23,
            place,
            rounds_played: 1,
            hole_scores: vec![
                HoleScore {
                    par: 223,
                    score: score1,
                    distance: Some(12),
                },
                HoleScore {
                    par: 125,
                    score: score2,
                    distance: Some(20),
                },
                HoleScore {
                    par: 75,
                    score: score3,
                    distance: Some(33),
                },
            ],
        }
    }

    #[test]
    fn test_combine_tournament_rounds() {
        let mut results: BTreeMap<u32, Vec<TournamentResult>> = BTreeMap::new();
        results.insert(
            1,
            vec![
                gen_tournament_result("PlayerA", 169, 211, 16, 1),
                gen_tournament_result("PlayerB", 198, 111, 69, 2),
                gen_tournament_result("PlayerC", 222, 133, 77, 4),
            ],
        );
        results.insert(
            2,
            vec![
                gen_tournament_result("PlayerA", 169, 211, 16, 2),
                gen_tournament_result("PlayerB", 198, 111, 69, 1),
                gen_tournament_result("PlayerC", 222, 133, 77, 4),
            ],
        );
        results.insert(
            3,
            vec![
                gen_tournament_result("PlayerA", 169, 211, 16, 1),
                gen_tournament_result("PlayerB", 198, 111, 69, 5),
                gen_tournament_result("PlayerC", 222, 133, 77, 8),
            ],
        );

        let combined = combine_tournament_rounds(results.clone());

        let expected_a = TournamentResult {
            pdga_number: Some(13216543),
            player_name: "PlayerA".to_string(),
            division: "Flying".to_string(),
            location: None,
            rating: None,
            round_rating: None,
            score: 1200,
            to_par: -69,
            place: 1,
            rounds_played: 1,
            hole_scores: vec![
                HoleScore {
                    par: 223,
                    score: 169,
                    distance: Some(12),
                },
                HoleScore {
                    par: 125,
                    score: 211,
                    distance: Some(20),
                },
                HoleScore {
                    par: 75,
                    score: 16,
                    distance: Some(33),
                },
                HoleScore {
                    par: 223,
                    score: 169,
                    distance: Some(12),
                },
                HoleScore {
                    par: 125,
                    score: 211,
                    distance: Some(20),
                },
                HoleScore {
                    par: 75,
                    score: 16,
                    distance: Some(33),
                },
                HoleScore {
                    par: 223,
                    score: 169,
                    distance: Some(12),
                },
                HoleScore {
                    par: 125,
                    score: 211,
                    distance: Some(20),
                },
                HoleScore {
                    par: 75,
                    score: 16,
                    distance: Some(33),
                },
            ],
        };

        let expected_b = TournamentResult {
            pdga_number: Some(13216543),
            player_name: "PlayerB".to_string(),
            division: "Flying".to_string(),
            location: None,
            rating: None,
            round_rating: None,
            score: 1200,
            to_par: -69,
            place: 5,
            rounds_played: 1,
            hole_scores: vec![
                HoleScore {
                    par: 223,
                    score: 198,
                    distance: Some(12),
                },
                HoleScore {
                    par: 125,
                    score: 111,
                    distance: Some(20),
                },
                HoleScore {
                    par: 75,
                    score: 69,
                    distance: Some(33),
                },
                HoleScore {
                    par: 223,
                    score: 198,
                    distance: Some(12),
                },
                HoleScore {
                    par: 125,
                    score: 111,
                    distance: Some(20),
                },
                HoleScore {
                    par: 75,
                    score: 69,
                    distance: Some(33),
                },
                HoleScore {
                    par: 223,
                    score: 198,
                    distance: Some(12),
                },
                HoleScore {
                    par: 125,
                    score: 111,
                    distance: Some(20),
                },
                HoleScore {
                    par: 75,
                    score: 69,
                    distance: Some(33),
                },
            ],
        };

        let expected_c = TournamentResult {
            pdga_number: Some(13216543),
            player_name: "PlayerC".to_string(),
            division: "Flying".to_string(),
            location: None,
            rating: None,
            round_rating: None,
            score: 1200,
            to_par: -69,
            place: 8,
            rounds_played: 1,
            hole_scores: vec![
                HoleScore {
                    par: 223,
                    score: 222,
                    distance: Some(12),
                },
                HoleScore {
                    par: 125,
                    score: 133,
                    distance: Some(20),
                },
                HoleScore {
                    par: 75,
                    score: 77,
                    distance: Some(33),
                },
                HoleScore {
                    par: 223,
                    score: 222,
                    distance: Some(12),
                },
                HoleScore {
                    par: 125,
                    score: 133,
                    distance: Some(20),
                },
                HoleScore {
                    par: 75,
                    score: 77,
                    distance: Some(33),
                },
                HoleScore {
                    par: 223,
                    score: 222,
                    distance: Some(12),
                },
                HoleScore {
                    par: 125,
                    score: 133,
                    distance: Some(20),
                },
                HoleScore {
                    par: 75,
                    score: 77,
                    distance: Some(33),
                },
            ],
        };

        assert_eq!(combined, vec![expected_a, expected_b, expected_c]);
    }

    #[test]
    fn test_convert_tournament_result() {
        let layout = gen_layout();
        let result = PDGATournamentResult {
            pdga_number: Some(13216543),
            player_name: "Evan Who Played Baseball with McBeast".to_string(),
            division: "Flying".to_string(),
            location: None,
            rating: None,
            round_rating: None,
            score: StupidAssAPIWTFPDDGA::U32(400),
            to_par: -23,
            rounds: "400,,,,,,,,,,,".to_string(),
            scores: "200,125,75".to_string(),
            pars: "223,125,75".to_string(),
            place: Some(1),
        };

        let expected =
            gen_tournament_result("Evan Who Played Baseball with McBeast", 200, 125, 75, 1);

        assert_eq!(convert_tournament_result(result, &layout), expected);
    }
}
