mod get_data;

use stats::TournamentData;

use ::log::error;
use actix_web::{get, middleware::Logger, web, App, HttpResponse, HttpServer, Responder};
use log::info;

#[get("/api/{pdga_id}")]
async fn api(id: web::Path<u32>) -> impl Responder {
    info!("Got request for tournament id: {}", id);
    let metadata = get_data::get_pdga_metadata(*id).await;

    return match metadata {
        Ok(m) => {
            let divisions = get_data::make_divisions(m.clone());

            info!("Getting results for tournament ID {}", id);
            let results = get_data::get_round_results(*id, divisions).await;

            match results {
                Ok(r) => {
                    let vec_of_results = get_data::build_tournament_results(r);
                    let response = TournamentData {
                        metadata: m,
                        data: vec_of_results,
                    };
                    info!("Success! Responding with data");
                    HttpResponse::Ok().json(response)
                }
                Err(e) => {
                    error!("Error in getting rounds.{:?}", e);
                    HttpResponse::NotFound().body(format!("{}", e))
                }
            }
        }
        Err(e) => {
            error!("Error in getting metadata {:?}", e);
            HttpResponse::NotFound().body(format!("{}", e))
        }
    };
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("debug"));

    HttpServer::new(|| App::new().wrap(Logger::default()).service(api))
        .bind(("0.0.0.0", 4343))?
        .run()
        .await
}
