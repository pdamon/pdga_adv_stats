use stats::{
    calc_player_statistics, PercentileStatistic, PlayerStatistics, RateStatistic, TournamentData,
    TournamentResult,
};

use gloo_net::http::Request;
use log::info;
use web_sys::HtmlInputElement;
use yew::prelude::*;

#[derive(Properties, PartialEq, Debug)]
struct TournamentInfoProps {
    name: String,
    dates: String,
    location: String,
}

#[derive(Properties, PartialEq, Debug)]
struct PlayerStatisticsProps {
    stats: PlayerStatistics,
}

#[derive(Properties, PartialEq, Debug)]
struct RoundResultsProps {
    divisions: Vec<Division>,
    rounds_played: Vec<RoundNumber>,
    results: Vec<TournamentResult>,
    onclick: Callback<TournamentResult>,
}

#[derive(PartialEq, Clone, Debug)]
struct Division {
    name: String,
    selected: bool,
}

#[derive(PartialEq, Clone, Debug)]
struct RoundNumber {
    number: u32,
    selected: bool,
}

#[derive(Properties, PartialEq, Debug)]
struct SidebarProps {
    divisions: Vec<Division>,
    rounds: Vec<RoundNumber>,
    request_status: Option<DataRequestStatus>,
    click_division: Callback<Event>,
    click_round: Callback<Event>,
    oninput: Callback<InputEvent>,
    onsubmit: Callback<MouseEvent>,
}

#[derive(PartialEq, Clone, Debug)]
enum DataRequestStatus {
    Success,
    Loading,
    Failed,
}

#[function_component(Sidebar)]
fn sidebar(props: &SidebarProps) -> Html {
    html!(
        <div id="sidebar">
            <p>{
                "Enter the Event ID for the event that you would like to search.
                For example, the 2022 Pro World Championships can be viewed by visiting the following link:"
            }</p>
                <a href="https://www.pdga.com/tour/event/55460">{ "https://www.pdga.com/tour/event/55460" }</a>
            <p>{
                "Therefore, to pull statistics for the 2022 Pro World Championships, you should enter Event ID 55460."
            }</p>
            <div id="get-tournament-info">
                <form id="get-info-form">
                    <input id="tournament-id-input" type="number" oninput={ props.oninput.clone() } />
                    <button id="update-button" onclick={ props.onsubmit.clone() }>{ "Update Tournament" }</button>
                </form>
                {
                    match props.request_status {
                        Some(DataRequestStatus::Success) => { html!(
                        <div id="request-status" style="background-color:green">
                            { "DATA LOADED SUCCESSFULLY!" }
                        </div>)
                        },
                        Some(DataRequestStatus::Loading) => { html!(
                        <div id="request-status" style="background-color:yellow">
                            { "LOADING DATA..." }
                        </div>)
                        },
                        Some(DataRequestStatus::Failed) => { html!(
                        <div id="request-status" style="background-color:red">
                            { "DATA LOADING FAILED!" }
                        </div>)
                        },
                        None => {html!(
                        <div id="request-status"></div>)
                        },
                    }
                }
            </div>
            <div id="divisions-selector">
                <h3 class="sidebar-title">{ "Divisions" }</h3>
                {
                    props.divisions
                        .iter()
                        .map(|division| {
                            let checked_callback = {
                                let clickdivision = props.click_division.clone();
                                Callback::from(move |e: Event| {
                                    clickdivision.emit(e);
                                })
                            };

                            html!(
                                <div id="checkbox-holder">
                                    <input
                                        type="checkbox"
                                        id={format!("checkbox-{}", division.name)}
                                        class="check-box"
                                        checked = { division.selected }
                                        onchange = { checked_callback } />
                                    <label for={format!("checkbox-{}", division.name)}>{ division.name.clone() }</label>
                                </div>
                        )
                    }).collect::<Vec<Html>>()
                }
            </div>
            <div id="rounds-played-selector">
                <h3 class="sidebar-title">{ "Rounds Played" }</h3>
                {
                    props.rounds
                        .iter()
                        .map(|round| {
                            let checked_callback = {
                                let clickround = props.click_round.clone();
                                Callback::from(move |e: Event| {
                                    clickround.emit(e);
                                })
                            };

                            html!(
                                <div class="checkbox-holder">
                                    <input
                                        type="checkbox"
                                        id={format!("checkbox-{}", round.number)}
                                        class="check-box"
                                        checked = { round.selected }
                                        onchange = { checked_callback }/>
                                    <label for={format!("checkbox-{}", round.number)}>{ round.number }</label>
                                </div>
                        )
                    }).collect::<Vec<Html>>()
                }
            </div>
        </div>
    )
}

#[function_component(PlayerStats)]
fn player_statistics(statistics: &PlayerStatisticsProps) -> Html {
    html! {
        <div id="player-statistics">
            <div id="player-data">
                <div id="player-name">
                    <h3>{ statistics.stats.name.clone() }</h3>
                </div>
                <div id="player-location">
                    <h3>{ statistics.stats.location.clone() }</h3>
                </div>
            </div>
            <div id="stat-wheels-wrapper">
                <div id="par-stats">
                    <div id="to-par-rates">
                        { to_par_stats_wheel(&statistics.stats.birdies, "Birdies") }
                        { to_par_stats_wheel(&statistics.stats.bogeys, "Bogeys") }
                    </div>
                    <div id="adv-par-rates">
                        { adv_par_stats_wheel(&statistics.stats.bob_plus, "BOB+") }
                        { adv_par_stats_wheel(&statistics.stats.bow_plus, "BOW+") }
                    </div>
                </div>
                <div id="split-stats">
                    <div id="distance-stats">
                        { strokes_gained_stat_wheel(&statistics.stats.sg_short, &format!("SG {}-{}", &statistics.stats.min, &statistics.stats.low - 1)) }
                        { strokes_gained_stat_wheel(&statistics.stats.sg_mid, &format!("SG {}-{}", &statistics.stats.low, &statistics.stats.high - 1)) }
                        { strokes_gained_stat_wheel(&statistics.stats.sg_long, &format!("SG {}-{}", &statistics.stats.high, &statistics.stats.max)) }
                    </div>
                    <div id="by-par-stats">
                        { strokes_gained_stat_wheel(&statistics.stats.sg_par_3s, "SG Par 3s") }
                        { strokes_gained_stat_wheel(&statistics.stats.sg_par_4s, "SG Par 4s") }
                        { strokes_gained_stat_wheel(&statistics.stats.sg_par_5s, "SG Par 5s") }
                    </div>
                </div>
            </div>
        </div>
    }
}

fn to_par_stats_wheel(stat: &RateStatistic, title: &str) -> Html {
    let wheel_angle = (stat.percentile_stat.value * 360.0 / 100.0).round();
    let wheel_color = if stat.percentile_stat.percentile > 200.0 / 3.0 {
        "green "
    } else if stat.percentile_stat.percentile < 100.0 / 3.0 {
        "red"
    } else {
        "blue"
    };
    html!(
        <div class="stat-wheel">
            <div class="stat-wheel-label">{ title }</div>
            <div style={format!("background-image: conic-gradient({0} 0deg, {0} {1}deg, lightgray 0deg", wheel_color, wheel_angle)} class="stat-wheel-ring">
                <div class="stat-middle">
                    <div class="stat-stat">{ format!("{}/{}", stat.successes, stat.attempts) }</div>
                    <div class="stat-stat">{ format!("{:.0}%", stat.percentile_stat.value) }</div>
                    <div class="stat-stat">{ print_percentile(stat.percentile_stat.percentile) }</div>
                </div>
            </div>
        </div>
    )
}

fn adv_par_stats_wheel(stat: &PercentileStatistic, title: &str) -> Html {
    let wheel_angle = (stat.percentile * 360.0 / 100.0).round();
    let wheel_color = if wheel_angle > 240.0 {
        "green "
    } else if wheel_angle < 120.0 {
        "red"
    } else {
        "blue"
    };
    html!(
        <div class="stat-wheel">
            <div class="stat-wheel-label">{ title }</div>
            <div style={format!("background-image: conic-gradient({0} 0deg, {0} {1}deg, lightgray 0deg", wheel_color, wheel_angle)} class="stat-wheel-ring">
                <div class="stat-middle">
                    <div class="stat-stat">{ format!("{:.0}", stat.value) }</div>
                    <div class="stat-stat">{ print_percentile(stat.percentile) }</div>
                </div>
            </div>
        </div>
    )
}

fn strokes_gained_stat_wheel(stat: &PercentileStatistic, title: &str) -> Html {
    let wheel_angle = (stat.percentile * 360.0 / 100.0).round();
    let wheel_color = if wheel_angle > 240.0 {
        "green "
    } else if wheel_angle < 120.0 {
        "red"
    } else {
        "blue"
    };
    html!(
        <div class="stat-wheel">
            <div class="stat-wheel-label">{ title }</div>
            <div style={format!("background-image: conic-gradient({0} 0deg, {0} {1}deg, lightgray 0deg", wheel_color, wheel_angle)} class="stat-wheel-ring">
                <div class="stat-middle">
                    <div class="stat-stat">{ format!("{:.2}", stat.value) }</div>
                    <div class="stat-stat">{ print_percentile(stat.percentile) }</div>
                </div>
            </div>
        </div>
    )
}
fn print_percentile(p: f32) -> String {
    match p.round() as u32 % 10 {
        1 => format!("{:.0}st", p),
        2 => format!("{:.0}nd", p),
        3 => format!("{:.0}rd", p),
        _ => format!("{:.0}th", p),
    }
}

#[function_component(TournamentInfo)]
fn tournament_info(
    TournamentInfoProps {
        name,
        dates,
        location,
    }: &TournamentInfoProps,
) -> Html {
    html! {
        <div id="tournament-info">
            <h1 id="tournament-name">{ name.clone() }</h1>
            <div id="tournament-date-location">
                <div id="tournament-date">
                    <h2>{ dates.clone() }</h2>
                </div>
                <div id="tournament-location">
                    <h2>{ location.clone() }</h2>
                </div>
            </div>
        </div>
    }
}

fn filter_results(
    results: &Vec<TournamentResult>,
    divisions: &Vec<Division>,
    rounds: &Vec<RoundNumber>,
) -> Vec<TournamentResult> {
    let result_filter_rounds = results
        .iter()
        .filter(|x| {
            rounds
                .iter()
                .find(|y| x.rounds_played == y.number)
                .unwrap()
                .selected
        })
        .collect::<Vec<_>>();

    let result_filter_divisions = result_filter_rounds
        .iter()
        .filter(|x| {
            divisions
                .iter()
                .find(|y| x.division == y.name)
                .unwrap()
                .selected
        })
        .collect::<Vec<_>>();

    let mut return_value = Vec::new();
    result_filter_divisions
        .iter()
        .map(|x| return_value.push((***x).clone()))
        .count();

    return return_value;
}

#[function_component(RoundResults)]
fn round_results(
    RoundResultsProps {
        divisions,
        rounds_played,
        results,
        onclick,
    }: &RoundResultsProps,
) -> Html {
    let onclick = onclick.clone();

    let filtered_results = filter_results(results, divisions, rounds_played);

    html! {
        <div id="player-results">
            <table id="results-table">
                <tr>
                    <th class="table-header-data">{ "Overall Place" }</th>
                    <th class="table-header-data">{ "Division Place" }</th>
                    <th class="table-header-data">{ "Name" }</th>
                    <th class="table-header-data">{ "Division" }</th>
                    <th class="table-header-data">{ "Rounds Played" }</th>
                    <th class="table-header-data">{ "Score" }</th>
                    <th class="table-header-data">{ "To Par" }</th>
                    <th class="table-header-data">{ "Tournament Rating" }</th>
                </tr>
            {
                filtered_results
                .iter()
                .enumerate()
                .map(|(index, result)| {
                    let on_player_select = {
                        let onclick = onclick.clone();
                        let player = result.clone();

                        Callback::from(move |_| {
                            onclick.emit(player.clone())
                        })
                    };

                    let overall_place = filtered_results.clone().iter().filter(|x| x.to_par < result.to_par).count() + 1;

                    html! {
                        <tr onclick={ on_player_select }
                            class={ format!("table-row {}", if (index + 1) % 2 == 0 { "table-row-even" } else { "table-row-odd" } ) }>
                            <td class="table-data">{ overall_place.clone() }</td>
                            <td class="table-data">{ result.place.clone() }</td>
                            <td class="table-data">{ result.player_name.clone() }</td>
                            <td class="table-data">{ result.division.clone() }</td>
                            <td class="table-data">{ result.rounds_played.clone() }</td>
                            <td class="table-data">{ result.score.clone() }</td>
                            <td class="table-data">{ match result.to_par.clone() { 0 => "E".to_string(), v => v.to_string() } }</td>
                            <td class="table-data">{ result.round_rating.clone().unwrap_or(0)}</td>
                        </tr>
                    }
                }).collect::<Vec<Html>>()
            }
            </table>
        </div>
    }
}

#[function_component(App)]
fn app() -> Html {
    // properties for update tournament search box
    let tournament_id_new: UseStateHandle<u32> = use_state(|| 0);
    let tournament_id_search: UseStateHandle<Option<u32>> = use_state(|| None);
    let stats_retrieval_status: UseStateHandle<Option<DataRequestStatus>> = use_state(|| None);

    // properties for selecting divisions and rounds played
    let divisions: UseStateHandle<Vec<Division>> = use_state(|| vec![]);
    let rounds_played: UseStateHandle<Vec<RoundNumber>> = use_state(|| vec![]);

    // properties for tournament data and results
    let tournament_metadata = use_state(|| TournamentInfoProps {
        name: "".to_string(),
        dates: "".to_string(),
        location: "".to_string(),
    });
    let tournament_results = use_state(|| vec![]);

    // properties for current player stats
    let current_player_stats = use_state(|| PlayerStatistics::new());

    // callbacks to update the current tournament id
    let on_input_tournament_id = {
        let tournament_id = tournament_id_new.clone();
        Callback::from(move |e: InputEvent| {
            let input: HtmlInputElement = e.target_unchecked_into();
            let new_tournament_id: u32 = input.value().parse().unwrap();
            tournament_id.set(new_tournament_id);
        })
    };

    let on_id_submit = {
        let tournament_id_new = tournament_id_new.clone();
        let tournament_id_search = tournament_id_search.clone();

        Callback::from(move |e: MouseEvent| {
            e.prevent_default();
            tournament_id_search.set(Some(*tournament_id_new).clone());
        })
    };

    // callback to update player statistics
    let on_player_select = {
        let current_player_stats = current_player_stats.clone();
        let results = tournament_results.clone();
        let divisions = divisions.clone();
        let rounds_played = rounds_played.clone();

        let filtered_results = filter_results(&(*results), &(*divisions), &(*rounds_played));

        Callback::from(move |player: TournamentResult| {
            current_player_stats.set(calc_player_statistics(
                filtered_results.clone(),
                player.player_name,
            ));
        })
    };

    // callbacks to update selected divisions and rounds played
    let on_division_checkbox = {
        let divisions = divisions.clone();

        Callback::from(move |x: Event| {
            x.prevent_default();

            let mut mut_division = (*divisions).clone();

            let element: HtmlInputElement = x.target_unchecked_into();
            let division = element.id().split("-").last().unwrap().to_string();

            let mut current_division = mut_division
                .iter()
                .find(|x| x.name == division)
                .unwrap()
                .clone();
            current_division.selected = !current_division.selected;

            mut_division.retain(|x| x.name != current_division.name);
            mut_division.push(current_division);

            mut_division.sort_by(|a, b| a.name.partial_cmp(&b.name).unwrap());

            divisions.set(mut_division);
        })
    };

    let on_round_checkbox = {
        let rounds = rounds_played.clone();

        Callback::from(move |x: Event| {
            x.prevent_default();

            let mut mut_round = (*rounds).clone();

            let element: HtmlInputElement = x.target_unchecked_into();
            let round: u32 = element.id().split("-").last().unwrap().parse().unwrap();

            let mut current_round = mut_round
                .iter()
                .find(|x| x.number == round)
                .unwrap()
                .clone();
            current_round.selected = !current_round.selected;

            mut_round.retain(|x| x.number != current_round.number);
            mut_round.push(current_round);

            mut_round.sort_by(|a, b| a.number.partial_cmp(&b.number).unwrap());

            rounds.set(mut_round);
        })
    };

    // handle getting new data from the backed when the selected ID changes
    let tmm = tournament_metadata.clone();
    let trm = tournament_results.clone();
    let tim = tournament_id_search.clone();
    let dm = divisions.clone();
    let rpm = rounds_played.clone();
    let srs = stats_retrieval_status.clone();

    use_effect_with_deps(
        move |_| {
            let tournament_metadata = tmm.clone();
            let tournament_results = trm.clone();
            let tournament_id = tim.clone();
            let divisions = dm.clone();
            let rounds_played = rpm.clone();
            let stats_retrieval_status = srs.clone();

            match *tournament_id {
                Some(tournament_id) => {
                    wasm_bindgen_futures::spawn_local(async move {
                        stats_retrieval_status.set(Some(DataRequestStatus::Loading));
                        info!("Getting data from /api/{tournament_id}");
                        let fetched_data_request =
                            Request::get(&format!("/api/{tournament_id}")).send().await;
                        match fetched_data_request {
                            Ok(v) => match v.json::<TournamentData>().await {
                                Ok(mut fetched_data) => {
                                    fetched_data.data.sort_by(|a, b| {
                                        if a.rounds_played == b.rounds_played {
                                            if a.to_par == b.to_par {
                                                a.place.partial_cmp(&b.place).unwrap()
                                            } else {
                                                a.to_par.partial_cmp(&b.to_par).unwrap()
                                            }
                                        } else {
                                            b.rounds_played.partial_cmp(&a.rounds_played).unwrap()
                                        }
                                    });

                                    let mut divisions_temp: Vec<Division> = vec![];
                                    let mut rounds_played_temp: Vec<RoundNumber> = vec![];

                                    fetched_data
                                        .data
                                        .iter()
                                        .map(|x| {
                                            if divisions_temp
                                                .iter()
                                                .find(|y| (**y).name == x.division)
                                                .is_none()
                                            {
                                                divisions_temp.push(Division {
                                                    name: x.division.clone(),
                                                    selected: true,
                                                });
                                            }
                                        })
                                        .count();

                                    fetched_data
                                        .data
                                        .iter()
                                        .map(|x| {
                                            if rounds_played_temp
                                                .iter()
                                                .find(|y| (**y).number == x.rounds_played)
                                                .is_none()
                                            {
                                                rounds_played_temp.push(RoundNumber {
                                                    number: x.rounds_played,
                                                    selected: true,
                                                });
                                            }
                                        })
                                        .count();

                                    divisions_temp
                                        .sort_by(|a, b| a.name.partial_cmp(&b.name).unwrap());
                                    rounds_played_temp
                                        .sort_by(|a, b| a.number.partial_cmp(&b.number).unwrap());

                                    tournament_metadata.set(TournamentInfoProps {
                                        name: fetched_data.metadata.name.clone(),
                                        dates: fetched_data.metadata.date_range.clone(),
                                        location: fetched_data.metadata.location.clone(),
                                    });
                                    tournament_results.set(fetched_data.data);
                                    divisions.set(divisions_temp.clone());
                                    rounds_played.set(rounds_played_temp.clone());
                                    stats_retrieval_status.set(Some(DataRequestStatus::Success));
                                    info!("Data retrieved successfully");
                                }
                                Err(e) => {
                                    stats_retrieval_status.set(Some(DataRequestStatus::Failed));
                                    info!("Failed to retrieve data with error:\n{e}");
                                }
                            },
                            Err(e) => {
                                stats_retrieval_status.set(Some(DataRequestStatus::Failed));
                                info!("Failed to retrieve data with error:\n{e}");
                            }
                        }
                    });
                }
                None => {}
            }
        },
        tournament_id_search,
    );

    // render the page
    html! {
        <>
            <div id="title">
                <h1>{ "Disc Golf Tournament Statistics" }</h1>
            </div>
            <div id="stage">
                <Sidebar
                    divisions={ (*divisions).clone() }
                    rounds={ (*rounds_played).clone() }
                    request_status={ (*stats_retrieval_status).clone() }
                    click_division={ on_division_checkbox.clone() }
                    click_round={ on_round_checkbox.clone() }
                    oninput={ on_input_tournament_id.clone() }
                    onsubmit={ on_id_submit.clone() }/>
                <div id="data-wrapper">
                    <TournamentInfo
                        name={ tournament_metadata.name.clone() }
                        dates={ tournament_metadata.dates.clone() }
                        location={ tournament_metadata.location.clone() } />
                    <PlayerStats
                        stats={ (*current_player_stats).clone() } />
                    <RoundResults
                        divisions={ (*divisions).clone() }
                        rounds_played={ (*rounds_played).clone() }
                        results={ (*tournament_results).clone() }
                        onclick={ on_player_select.clone() }/>
                </div>
            </div>
        </>
    }
}

fn main() {
    wasm_logger::init(wasm_logger::Config::new(log::Level::Debug));
    yew::Renderer::<App>::new().render();
}
