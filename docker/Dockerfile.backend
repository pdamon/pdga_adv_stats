FROM registry.gitlab.com/pdamon/pdga_adv_stats/ci:latest AS build

WORKDIR /app

COPY Cargo.toml Cargo.lock /app/

RUN cargo new --lib /app/stats
COPY stats/Cargo.toml /app/stats/

RUN cargo new /app/backend
COPY backend/Cargo.toml /app/backend/

RUN cargo new /app/frontend
COPY frontend/Cargo.toml /app/frontend/

WORKDIR /app/backend
RUN --mount=type=cache,target=/usr/local/cargo/registry cargo build --release

COPY ./stats /app/stats
COPY ./backend /app/backend


RUN --mount=type=cache,target=/usr/local/cargo/registry <<EOF
  set -e
  touch /app/stats/src/lib.rs /app/backend/src/main.rs
  cargo build --release
EOF

FROM debian:stable-slim

RUN apt update && apt install -y libssl3 ca-certificates

WORKDIR /app

COPY --from=build /app/target/release/backend .

CMD ["/app/backend"]
