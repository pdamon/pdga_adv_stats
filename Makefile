registry_url := registry.gitlab.com/pdamon/pdga_adv_stats
current_ci_tag := 0.1.1
current_app_tag := 0.1.1

# THIS STORES PASSWORD IN PLAIN TEXT UNTIL YOU LOGOUT SO BE CAREFUL
docker-login:
	docker login registry.gitlab.com

docker-build-ci:
	docker build -f docker/Dockerfile.ci -t $(registry_url)/ci:$(current_ci_tag) .
	docker build -f docker/Dockerfile.ci -t $(registry_url)/ci:latest .

docker-build-app:
	docker build -f docker/Dockerfile.backend -t $(registry_url)/backend:$(current_app_tag) .
	docker build -f docker/Dockerfile.frontend -t $(registry_url)/frontend:$(current_app_tag) .
	docker build -f docker/Dockerfile.backend -t $(registry_url)/backend:latest .
	docker build -f docker/Dockerfile.frontend -t $(registry_url)/frontend:latest .

docker-push: docker-build-ci docker-build-app
	docker push $(registry_url)/ci:$(current_ci_tag)
	docker push $(registry_url)/backend:$(current_app_tag)
	docker push $(registry_url)/frontend:$(current_app_tag)
	docker push $(registry_url)/ci:latest
	docker push $(registry_url)/backend:latest
	docker push $(registry_url)/frontend:latest

up:
	docker compose -f docker/docker-compose.yaml up -d

down:
	docker compose -f docker/docker-compose.yaml down

clean:
	rm -rf target/ dist/

# Adding the RUSTC_BOOTSTRAP=1 makes grcov work without being on nightly
# see https://github.com/mozilla/grcov#example-how-to-generate-gcda-files-for-a-rust-project
build:
	RUSTFLAGS="-Zprofile -Ccodegen-units=1 -Copt-level=0 -Clink-dead-code -Coverflow-checks=off -Zpanic_abort_tests -Cpanic=abort" \
	CARGO_INCREMENTAL=0 \
	RUSTDOCFLAGS="-Cpanic=abort" \
	RUSTC_BOOTSTRAP=1 \
	cargo build --workspace --verbose

test:
	RUSTFLAGS="-Zprofile -Ccodegen-units=1 -Copt-level=0 -Clink-dead-code -Coverflow-checks=off -Zpanic_abort_tests -Cpanic=abort" \
	CARGO_INCREMENTAL=0 \
	RUSTDOCFLAGS="-Cpanic=abort" \
	RUSTC_BOOTSTRAP=1 \
	cargo test --workspace --verbose

grcov:
	grcov . -s . --binary-path ./target/debug/ -t html --branch --ignore-not-existing -o ./target/debug/coverage/

docs:
	cargo doc

fmt:
	cargo fmt --all

check-fmt:
	cargo fmt --all -- --check

